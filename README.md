# README #

## Parking Ticket System ##

### Maven ###

```
#!unix

mvn jetty:run
```
### API ###

* POST	localhost:8080/parking-ticket-system/parking/new
* GET	localhost:8080/parking-ticket-system//parking
* GET	localhost:8080/parking-ticket-system/parking/cost/{id}
* PUT	localhost:8080/parking-ticket-system/parking/close/{id}

### Package ###

* Jetty
* Jersy Core
* Jersy-bundle


### IDE ###
* Eclipse Mars