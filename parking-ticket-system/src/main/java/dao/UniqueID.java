package dao;

import java.util.concurrent.atomic.AtomicInteger;

public class UniqueID {
	
	private static final AtomicInteger uniqueID = new AtomicInteger(0);
	
	
	public static String getUniqueID () {
    	
    	String s = Integer.toString(uniqueID.incrementAndGet());
    	return s;
    }

}
