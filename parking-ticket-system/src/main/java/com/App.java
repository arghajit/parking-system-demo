package com;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.Parking;
import dao.AppDAO;

/*
 * POST	localhost:8080/parking-ticket-system/parking/new -> add new entry
 * GET	localhost:8080/parking-ticket-system//parking -> get current system status
 * GET	localhost:8080/parking-ticket-system/parking/cost/{id} -> get cost till current time
 * PUT	localhost:8080/parking-ticket-system/parking/close/{id} -> exit the vehicle and print cost
 * 
 */

@Path("/parking")
public class App {
		
	@GET
    @Produces({ MediaType.TEXT_PLAIN})
    public String getDefault() {

		String s = AppDAO.getCount() + " vehicles in parking now. Choose your action.";
		return s;
		
    }
	
    @POST
    @Path("/new")
    @Produces({ MediaType.TEXT_PLAIN})
    public String addNewParking(String nullvalue) {

		String s = "Entry added. ID is {" + AppDAO.addEntry()+"}.";
		return s;
    }
	
	
	@GET
    @Path("/cost/{parkingNo}")
    @Produces({ MediaType.TEXT_PLAIN})
    public String getCost(@PathParam("parkingNo") String parkingNo) {
        
		String s = "Parking cost till now is $"+AppDAO.getCurrentCost(parkingNo) + ".";
		return s;
    }
	
	@PUT
    @Path("/close/{parkingNo}")
    @Produces({ MediaType.TEXT_PLAIN})
	public String getCloseEntry(@PathParam("parkingNo") String parkingNo) {


		String s = "Vehicle exited. Parking cost is $"+AppDAO.closeEntry(parkingNo);
		return s;		
    }
	
	@GET
	@Path("/all")
    @Produces({ MediaType.APPLICATION_XML })
    public List<Parking> getAllEntries() {
        List<Parking> list = AppDAO.getAllEntries();
        return list;
    }


}
