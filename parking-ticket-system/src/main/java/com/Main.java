package com;

import static spark.Spark.*;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.servlet.SparkApplication;

public class Main implements SparkApplication {
//    public static void main(String[] args) {
//    	get("hello", (req, res) -> "Hello World");
//    }
    @Override
    public void init() {
        get("/", (request, response) -> "Hello World");

        get("/hello/:name", (request, response) -> {
            return "Hello: " + request.params(":name");
        });
    }

}